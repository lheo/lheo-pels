all: target/pel-0002.html target/pel-0004.html \
	target/pel-0005.html target/pel-0006.html target/pel-0007.html \
	target/pel-0008.html target/pel-0009.html target/pel-0010.html \
	target/pel-0011.html target/pel-0012.html target/pel-0013.html \
	target/pel-0014.html target/pel-0015.html target/pel-0016.html \
	target/pel-0017.html target/pel-0018.html target/pel-0019.html \
	target/pel-0020.html target/pel-0022.html target/pel-0025.html \
	target/pel-0026.html target/pel-0027.html target/pel-0028.html \
	target/pel-0029.html target/pel-0030.html target/pel-0031.html \
	target/pel-0032.html target/pel-0033.html target/pel-0034.html \
	target/pel-0035.html target/pel-0036.html target/pel-0037.html \
	target/pel-0038.html \
	target/index.html

target/pel-%.html: src/pel-%.md
	@mkdir -p target
	pandoc -o $@ $< 

src/pel-%.md: raw/pel-%.gdoc
	@mkdir -p src 
	python tools/google-docs-sync/to_md.py $< $@

target/index.html: templates/pelindex.html tools/create_index.py $(wildcard src/pel-*.md) $(wildcard src/pel-*.pdf)
	python tools/create_index.py --template pelindex.html \
        --templates-dir templates \
        $@ src
	@cp src/*.pdf target

target/pel-0002.html: src/pel-0002.md
# Pas de PEL 3
target/pel-0004.html: src/pel-0004.md
target/pel-0005.html: src/pel-0005.md
target/pel-0006.html: src/pel-0006.md
target/pel-0007.html: src/pel-0007.md
target/pel-0008.html: src/pel-0008.md
target/pel-0009.html: src/pel-0009.md
target/pel-0010.html: src/pel-0010.md
target/pel-0011.html: src/pel-0011.md
target/pel-0012.html: src/pel-0012.md
target/pel-0013.html: src/pel-0013.md
target/pel-0014.html: src/pel-0014.md
target/pel-0015.html: src/pel-0015.md
target/pel-0016.html: src/pel-0016.md
target/pel-0017.html: src/pel-0017.md
target/pel-0018.html: src/pel-0018.md
target/pel-0019.html: src/pel-0019.md
target/pel-0020.html: src/pel-0020.md
# Pas de PEL 21
target/pel-0022.html: src/pel-0022.md
# Pas de PEL 23, 24
target/pel-0025.html: src/pel-0025.md
target/pel-0026.html: src/pel-0026.md
target/pel-0027.html: src/pel-0027.md
target/pel-0028.html: src/pel-0028.md
target/pel-0029.html: src/pel-0029.md
target/pel-0030.html: src/pel-0030.md
target/pel-0031.html: src/pel-0031.md
target/pel-0032.html: src/pel-0032.md
target/pel-0033.html: src/pel-0033.md
target/pel-0034.html: src/pel-0034.md
target/pel-0035.html: src/pel-0035.md
target/pel-0036.html: src/pel-0036.md
target/pel-0037.html: src/pel-0037.md
target/pel-0038.html: src/pel-0038.md

raw/pel-0002.gdoc:
	@mkdir -p raw
	python tools/google-docs-sync/download.py \
		'1a_DE7MKUmM4SfGb_QAfV18debdl6g2hHjmJ6VKKC4ME'\
		raw/pel-0002.gdoc -c tools/google-docs-sync

raw/pel-0004.gdoc:
	@mkdir -p raw
	python tools/google-docs-sync/download.py \
		'1DMJbkVLtRc61ggI9NMaubk6u7q8JWmpirxAwD9NWrXk'\
		$@ -c tools/google-docs-sync

raw/pel-0005.gdoc:
	@mkdir -p raw
	python tools/google-docs-sync/download.py \
		'1bn7RRx4Qhl-78XWkb1X_R8zH9Jl6ngNJZM5XQjLuxqM'\
		$@ -c tools/google-docs-sync

raw/pel-0006.gdoc:
	@mkdir -p raw
	python tools/google-docs-sync/download.py \
		'1xzu9JuXWxZ6_Ct6EWUMQhKnIQBSAeFBW61-qYwQUAdk'\
		$@ -c tools/google-docs-sync

raw/pel-0007.gdoc:
	@mkdir -p raw
	python tools/google-docs-sync/download.py \
		'1n1pKM75-yJExx0kQHEJCOHZFK2tdXz7MTNuODyNJzu4'\
		$@ -c tools/google-docs-sync

raw/pel-0008.gdoc:
	@mkdir -p raw
	python tools/google-docs-sync/download.py \
		'1iBvg3ezJFb7y-pXGg4vMqUoCUDTc4t9scSOtRxQjvq8'\
		$@ -c tools/google-docs-sync

raw/pel-0009.gdoc:
	@mkdir -p raw
	python tools/google-docs-sync/download.py \
		'1RLF1Q9PLwALJsvT-thmnVYsu8wZPmQsQpvy-vejDo0s'\
		$@ -c tools/google-docs-sync

raw/pel-0010.gdoc:
	@mkdir -p raw
	python tools/google-docs-sync/download.py \
		'1FWkQ88UPIISfsvUNthuRy8w-ciisnvSCkGH0b99Yjx8'\
		$@ -c tools/google-docs-sync

raw/pel-0011.gdoc:
	@mkdir -p raw
	python tools/google-docs-sync/download.py \
		'18U4BvCpB8HwFnGvZI__NVQTQrhC33U_QvvTGr6DJ9Cw'\
		$@ -c tools/google-docs-sync

raw/pel-0012.gdoc:
	@mkdir -p raw
	python tools/google-docs-sync/download.py \
		'1zo5MgBLgu5ZAtFfwnrtIgXy4lak1JFHZqHQQD6tc_-A'\
		$@ -c tools/google-docs-sync

raw/pel-0013.gdoc:
	@mkdir -p raw
	python tools/google-docs-sync/download.py \
		'1ZQohv1k10QQd1k6xF1TwnwE-CBkmqYMmFwPsXXhpL7w'\
		$@ -c tools/google-docs-sync


raw/pel-0014.gdoc:
	@mkdir -p raw
	python tools/google-docs-sync/download.py \
		'1ip493_D8i-RXmEzkaDyl91P-PvhJV0jwjJ2LGXGlrc0'\
		$@ -c tools/google-docs-sync

raw/pel-0015.gdoc:
	@mkdir -p raw
	python tools/google-docs-sync/download.py \
		'1xj79Gyqyezui38G04zkkg4SgBfGs1EEbZ5OZ7Zp95Lc'\
		$@ -c tools/google-docs-sync

raw/pel-0016.gdoc:
	@mkdir -p raw
	python tools/google-docs-sync/download.py \
		'1cdNLWALEeYHfm0vpy5I0HYI_tqQMxmw3gI_JrzTabQ8'\
		$@ -c tools/google-docs-sync

raw/pel-0017.gdoc:
	@mkdir -p raw
	python tools/google-docs-sync/download.py \
		'1YXkS4-V8JVS7o2tiYZ10L7bkWa6lPu8pitRP4D8xFfM'\
		$@ -c tools/google-docs-sync

raw/pel-0018.gdoc:
	@mkdir -p raw
	python tools/google-docs-sync/download.py \
		'1cz7-mBHLr3pVfVHV-k77n8TtUPOVFeMif50Oz-37rgc'\
		$@ -c tools/google-docs-sync

raw/pel-0019.gdoc:
	@mkdir -p raw
	python tools/google-docs-sync/download.py \
		'1ZRsgDuCtOxAMZZw8UNfISDQ6pyk4gm8BzdWOKYfxxRo'\
		$@ -c tools/google-docs-sync

raw/pel-0020.gdoc:
	@mkdir -p raw
	python tools/google-docs-sync/download.py \
		'1SP7BzjBg-54qThJcX_pAJLyoySaAmMPScph4W42OaNs'\
		$@ -c tools/google-docs-sync

raw/pel-0022.gdoc:
	@mkdir -p raw
	python tools/google-docs-sync/download.py \
		'1-RQMGMz66ckHdLiJRhOTDhNkGBVzmQ-MArVSQYvE2Ew'\
		$@ -c tools/google-docs-sync

raw/pel-0025.gdoc:
	@mkdir -p raw
	python tools/google-docs-sync/download.py \
		'1ZhC4_VV8ttALS-7zEyQSNvtMk6MbbLTC1rn4gyQ_6Lk'\
		$@ -c tools/google-docs-sync

raw/pel-0026.gdoc:
	@mkdir -p raw
	python tools/google-docs-sync/download.py \
		'1SdehJCH5K3ZxjGP0YWvBcOcADgQvR1o0pbOOMazd5D4'\
		$@ -c tools/google-docs-sync

raw/pel-0027.gdoc:
	@mkdir -p raw
	python tools/google-docs-sync/download.py \
		'1xFVcJ_xAAGjQ9gJcYewv-6ZR8Yo3NJ7SqTjdcj8z5YU'\
		$@ -c tools/google-docs-sync

raw/pel-0028.gdoc:
	@mkdir -p raw
	python tools/google-docs-sync/download.py \
		'1IIuSHc68JdcXYX6UJiUpoRAvtAMSmuBW30ejtI7kqkw'\
		$@ -c tools/google-docs-sync

raw/pel-0029.gdoc:
	@mkdir -p raw
	python tools/google-docs-sync/download.py \
		'1DrYDZN1h7PStBzYqnfLtrzA4ChMHB7FiUiEaexqPHD4'\
		$@ -c tools/google-docs-sync

raw/pel-0030.gdoc:
	@mkdir -p raw
	python tools/google-docs-sync/download.py \
		'1JBFl5WGcLxQ7vMopNov4arqm75uXlNlDIbeOzezt4uk'\
		$@ -c tools/google-docs-sync

raw/pel-0031.gdoc:
	@mkdir -p raw
	python tools/google-docs-sync/download.py \
		'1aX4MD4Tb0GEGoxHAX0Cqx8OTTThSJKHaXlfmyvVjCqU'\
		$@ -c tools/google-docs-sync

raw/pel-0032.gdoc:
	@mkdir -p raw
	python tools/google-docs-sync/download.py \
		'1pnQ902YS3fbjYGcxmwhghmmzRDP_gDzo5oZzV0iFkqg'\
		$@ -c tools/google-docs-sync

raw/pel-0033.gdoc:
	@mkdir -p raw
	python tools/google-docs-sync/download.py \
		'1IhmJk0omT83IZm4qslvhvL82nACIaQ5tYaCPGAxhojM'\
		$@ -c tools/google-docs-sync

raw/pel-0034.gdoc:
	@mkdir -p raw
	python tools/google-docs-sync/download.py \
		'1U0xYsfCI9LQOsualtN_RVjfDLJUdOkQFUcvWZAqyf1I'\
		$@ -c tools/google-docs-sync

raw/pel-0035.gdoc:
	@mkdir -p raw
	python tools/google-docs-sync/download.py \
		'1b2mlR3XV-QP0zFNPnW9AUAUBHpFOOCmCjfe9XfL26HY'\
		$@ -c tools/google-docs-sync

raw/pel-0036.gdoc:
	@mkdir -p raw
	python tools/google-docs-sync/download.py \
		'1yQsHWQW6OW6EEGY8PaNKRttS3wIq2SW1alvN7B__M2U'\
		$@ -c tools/google-docs-sync

raw/pel-0037.gdoc:
	@mkdir -p raw
	python tools/google-docs-sync/download.py \
		'1G5uvbg4qpbLqLJ0CymLpsNLwchXsyt997696tvgoMpI'\
		$@ -c tools/google-docs-sync

raw/pel-0038.gdoc:
	@mkdir -p raw
	python tools/google-docs-sync/download.py \
		'1H6f3xv5pWY5FrXWHktyIoGFC8AK7pxZ70lspqIMRsfw'\
		$@ -c tools/google-docs-sync
