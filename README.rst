Propositions d'Évolution de LHÉO
================================

Les PEL de ce dépôt sont publiées sur le web sous https://dev.lheo.org/pels/.

Le format standard de rédaction d'une PEL est le format `reStructuredText <https://docutils.sourceforge.io/rst.html>`_, le format `Markdown <https://daringfireball.net/projects/markdown/syntax>`_ étant également supporté. Deux templates sont présents dans ce répertoire dans ces deux formats: ``pel-template.rst`` et ``pel-template.md``.

Transformation des PEL en HTML
------------------------------

Il est nécessaire d'avoir Python 3. Il est recommandé de créer un environnement virtuel::

    python3 -m venv ~/venv/lheo-pels
    source ~/venv/lheo-pels/bin/activate
    pip install --upgrade pip
    pip install --upgrade setuptools

Ensuite, les ``docutils`` sont nécessaires afin de générer le HTML::

    pip install -r requirements

Utilisation du Makefile
-----------------------

La cible par défaut va construire tous les fichiers HTML cibles, mais sans
aller récupérer en ligne les documents via l'API Google Docs où pourraient se trouver
des PEL rédigées dans ce format. 

Pour récupérer une nouvelle version des PEL en format Google Doc, qui seront transformées automatiquement en Markdown,
il est nécessaire d'effacer le ou les fichiers dont on souhaite le téléchargement dans le répertoire ``raw/`` et de taper ``make``.

