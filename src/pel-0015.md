# PEL-15 Multi Modalités FI/FA/CA/CP d'une session avec mixité des publics et cycles


Demandeur : Groupe Lheo Apprentissage

Rédacteur.trice.s : Corinne Haranger, Deborah MARCHAND  Cariforef  PDL pour RCO


## Motivation et description




Contexte.



Une session peut accueillir des publics mixtes avec des voies d'accès différentes et des ventilations de places différentes. La collecte d’une formation en apprentissage doit préciser  l'année d'inscription en apprentissage afin par ex de ne pas proposer une entrée en 1ère pour des élèves du palier seconde.Ce afin de connaître la durée de formation, afficher l’année de la formation en cours et s'assurer que l’année de formation est accessible en apprentissage 

Pour les formations qui se déroulent en 2 ans ou un 1 an, il est nécessaire d’afficher toutes les modalités possibles. 

Au total, il faut collecter 3 infos : durée, année inscription + année(s) ouverte(s) en apprentissage



Quelle nouvelle information ?

Cette PEL doit garder la logique de la PEL 2

Elle a aussi des impacts sur OF Financeur



Pourquoi ?

Pour gérer des sessions multi voie d’acces en ventilant les places et en gérant les cycles d’année.

Identification des consommateurs

MNA

PE

DGEFPIdentification des producteurs de l'information

OF


## Évolutions du langage proposées




Durée de cycle

Voie d’accès

Année de cycle



Auxquels s'ajoutent les notions de capacités min max décrites en PEL 2 


## Exemple de contenu




session    1..1

    periode    1..1

   	 debut    1..1

   	 fin    1..1

    duree-cycle    0..1

    

    <voie-acces>    

    <contrat-apprentissage>     0..1

   	 <voie>    1..1

   	 <capacite-min-accueil-garantie>    1..1

   	 <capacite-max-previsionnelle>    1..1

   	 <annees-cycle>    0..n

   		 <annee-cycle>    0..1

   		 <nb-places>    0..1

    type (FI-FC-CP)    0..1

   	 <capacite-min-accueil-garantie>    0..1

   	 <capacite-max-previsionnelle>    0..1

   	 <annees-cycle>    0..1

   		 <annee-cycle>    0..1

   		 <nb-places>    0..1


