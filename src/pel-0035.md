# PEL-35 Nouvel élément Enseignements



Nature de la proposition : ajout d’élément

Demandeur : ONISEP

Rédacteur.trice.s : ONISEP

Priorité MoSCoW : M

Impact SI producteur : nul

Impact SI collecteur : moyen

Impact SI diffuseur : moyen à fortStatut : brouillon

Date de création : 14 avril 2021

Date de modification : 



Motivation et description

Pouvoir afficher/exploiter les enseignements, matières, langues dispensés dans l’action de formation

Évolutions proposées

Ajouter :

une balise englobante <enseignements> sous <action>

une balise englobante <enseignement> sous <enseignements>

une balise <libelle>

une balise <type>

une balise <genre>

une balise <commentaire>



<action>

<enseignements>[0 ;1]

<enseignement>[1;n]

		<libelle>[1 ;1]

		<type>[1 ;1]

		<genre>[0;1]

		<commentaire>[0;1]

</enseignements>

Impacts


### Impacts sur les producteurs


?


### Impacts sur les systèmes d'information collecteurs


Modification du SI pour ingérer

Modification des flux de restitution




### Impacts sur les systèmes d'information diffuseurs


Modification des flux d’import, du SI et de l’affichage



Discussion


