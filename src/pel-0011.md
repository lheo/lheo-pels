# PEL-11 Recrutement


Demandeur : Groupe Lheo Apprentissage

Rédacteur.trice.s : Christophe Bonraisin <christ.bonraisin@pole-emploi.fr>, Mission ApprentissageNathalie Lavolo, Corinne Haranger, Deborah MARCHAND  Cariforef Ndie et PDL pour RCOPriorité MoSCoW : S

Impact producteur :Moyen

Impact collecteur : Fort

Impact diffuseur : Moyen

Statut : brouillon

Date de modification : 


## Motivation et description


Il s’agit de mieux gérer les différentes modalités de recrutement, les états du recrutement, les périodes de recrutements et les informations collectives à distance au niveau de la session. 



Créer un nouveau bloc Recrutement via une table avec liste fermée des ‘modalités de recrutement’ (tests, entretiens, dossiers..) et permettre l'ajout d’un bouton de prise de Rendez-vous (Lheo ?) .



Modalités de recrutement en table plutôt que champ texte avec valeurs à définir

Multivaleurs

Non obligatoire : [0;n ]

Prendre en compte les autres besoins qui ont émergé (Ouiform/Kairos) en matière de recrutement FPC

Info coll à distance

Période de recrutement : date/debut fin > Période d' inscription ? 

Entretien individuel > Modalités ? 

Inscription sur candidature > Modalités ? 




## Évolutions du langage proposées




Descendre les infos à la session et non plus au niveau de l’action et de l’organisme financeur.

Supprimer le code périmètre recrutement.

Garder ‘Etat recrutement’  mais le descendre au niveau de recrutement

Permettre l’Information collective à distance avec donc un lieu non obligatoire.Proposer une liste fermée de modalités de recrutement au niveau de la session pouvant  être décrites via un champ multi évalués .  

cv

lettre de motivation

méthode de recrutement par simulation

concours

dossier

entretien physique

entretien à distance

tests pratiques

épreuves écrites

entretien individuel ? 

inscription sur candidature ?   

Proposition d’un champ texte libre au niveau de la session (pour permettre la description de modalités qui ne peuvent se réduire aux tables proposées puis dans un 2d temps étude des champs texte libre utilisés pour la mise à jour éventuelle de la table fermée des modalités  )

Proposition d’ajout de champ période de recrutement avec date début et de fin à moins que la période d’inscription serve déjà à ca






























