import argparse
import sys
import os
import re

import jinja2

title_re = re.compile('# (?P<title>PEL.*)')


class PEL(object):

    def __init__(self, href, title, pdf):
        self.href = href
        self.title = title
        self.pdf = pdf

    def __repr__(self):
        return '%s %s' % (self.href, self.title)

    @classmethod
    def from_peldir(cls, peldir):
        pels = []
        files = filter(lambda x: x.endswith('.md'), os.listdir(peldir))
        files = sorted(files)
        for fname in files:
            with open(os.path.join(peldir, fname), 'r') as f:
                content = f.read()
                tm = title_re.search(content)
                assert tm is not None, fname
                title = tm.group('title')
                href = fname.replace('.md', '.html')
                pdf = fname.replace('.md', '.pdf')
                if not os.path.exists(os.path.join(peldir, pdf)):
                    pdf = None
                pel = cls(href, title, pdf)
                print(pel)
                pels.append(pel)
        return pels


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("targetfile", help="File to generate")
    parser.add_argument("peldir", help="Directory of PELs")
    parser.add_argument("--templates-dir", help="templates directory",
            required=True)
    parser.add_argument("--template", help="Template file",
            default="default.html")
    args = parser.parse_args()

    pels = PEL.from_peldir(args.peldir)

    template_loader = jinja2.FileSystemLoader(searchpath=args.templates_dir)
    template_env = jinja2.Environment(loader=template_loader)
    template = template_env.get_template(args.template)

    output_content = template.render(pels=pels)

    with open(args.targetfile, 'w') as f:
        f.write(output_content)

    return 0


if __name__ == '__main__':
    sys.exit(main())

