from __future__ import print_function
import os.path
import sys
import json


def parse_document(document):
    body = document.get('body')
    assert body
    content = body.get('content')
    assert content
    div = []
    for span in content:
        paragraph = span.get('paragraph')
        if not paragraph:
            continue
        elements = paragraph.get('elements')
        t = []
        for element in elements:
            textRun = element.get('textRun')
            if textRun:
                t.append(textRun.get('content', ''))
        paragraphStyle = paragraph.get('paragraphStyle')
        assert paragraphStyle
        namedStyleType = paragraphStyle.get('namedStyleType')
        div.append((namedStyleType, ''.join(t)))
    return div 


def pdocument_to_markdown(pdocument):
    d = []
    for item in pdocument:
        style = item[0]
        content = item[1]
        content = content.replace('\x0b', '')
        if style == 'TITLE':
            d.append('# '+ content)
            d.append('')
        elif style == 'HEADING_1':
            d.append('')
            d.append('## '+ content)
            d.append('')
        elif style == 'HEADING_2':
            d.append('')
            d.append('### '+ content)
            d.append('')
        elif style == 'HEADING_3':
            d.append('')
            d.append('#### '+ content)
            d.append('')
        elif style == 'NORMAL_TEXT':
            d.append(content)
    return d


def main():
    """Transform Google Docs to markdown."""
    source = sys.argv[1]
    target = sys.argv[2]

    with open(source, "r") as f:
        document = json.loads(f.read())

    from pprint import pprint
    div = parse_document(document)
    mddoc = pdocument_to_markdown(div)
    #pprint(mddoc)

    with open(target, "w") as f:
        f.write('\n'.join(mddoc))

    return 0


if __name__ == '__main__':
    main()

