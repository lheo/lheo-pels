from __future__ import print_function
import pickle
import os.path
import json
import sys
import argparse

from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request

# If modifying these scopes, delete the file token.pickle.
SCOPES = ['https://www.googleapis.com/auth/documents.readonly']


def main():
    """Download a document from Google Docs API and overwrite target
    if and only if it is different from it.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("docid")
    parser.add_argument("target")
    parser.add_argument("-c", "--credentials", help=("directory where"
            "credentials are stored (credentials.json and cached "
            "token.pickle)"), default=None)
    args = parser.parse_args()
    docid = args.docid 
    target = args.target 
    token_filename = 'token.pickle'
    creds_filename = 'credentials.json'
    if args.credentials:
        token_filename = os.path.join(args.credentials, 'token.pickle')
        cred_filename = os.path.join(args.credentials, 'credentials.json')

    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists(token_filename):
        with open(token_filename, 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                creds_filename, SCOPES)
            #creds = flow.run_local_server()
            creds = flow.run_console()
        # Save the credentials for the next run
        with open(token_filename, 'wb') as token:
            pickle.dump(creds, token)

    service = build('docs', 'v1', credentials=creds)

    # Retrieve the documents contents from the Docs service.
    document = service.documents().get(documentId=docid).execute()
    if not document:
        print("no content")
        return 1

    title = document.get('title')
    print('The title of the document is: {}'.format(title))

    new_content = json.dumps(document, indent=4)
    current_content = None
    if os.path.exists(target):
        with open(target, 'r') as f:
            current_content = f.read()
    if current_content is None or current_content != new_content:
        with open(target, 'w') as f:
            f.write(new_content)
            print("wrote: %s" % target)
    return 0


if __name__ == '__main__':
    main()

