python3 -m venv ~/venv/lheo-pels
source ~/venv/lheo-pels/bin/activate
pip install -r requirements.txt

credentials.json must be retrieved from Google API webpage.

Récupération du code exemple Python:

https://developers.google.com/docs/api/quickstart/python

Changement de run_local_server par run_console. Voir:

https://google-auth-oauthlib.readthedocs.io/en/latest/reference/google_auth_oauthlib.flow.html

To Download a document and dump it to disk:

python download.py DOCID targetname



