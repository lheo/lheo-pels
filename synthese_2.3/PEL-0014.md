# Résumé

Plusieurs organismes de formation peuvent intervenir sur une formation, notamment pour les achats groupés en matière de formation continue, donc aujourd’hui plusieurs Organismes de formation peuvent être formateurs. Une formation (une session unique avec un seul Organisme de formation) peut se dérouler sur plusieurs lieux et également se dérouler à distance, sans lieu de formation physique.

# Origine

- HTML: https://dev.lheo.org/pel/pel-0014.html
- Source: https://gitlab.com/lheo/lheo-pels/-/blob/master/src/pel-0014.md
- PDF: https://gitlab.com/lheo/lheo-pels/-/blob/master/src/pel-0014.pdf

# Solution technique proposée

- Changement de cardinalité de `<organisme-formateur>` dans `<action>` pour passer de 0,1 à 0,n
- Changement de cardinalité de `<lieu-de-formation>` de 1,1 à 0,n