# Résumé

Ajout d'un élément SIRET pour le lieu de la formation.

# Origine

- HTML: https://dev.lheo.org/pel/pel-0034.html
- Source: https://gitlab.com/lheo/lheo-pels/-/blob/master/src/pel-0034.md
- PDF: https://gitlab.com/lheo/lheo-pels/-/blob/master/src/pel-0034.pdf

# Solution technique proposée

```xml
<lieu-de-formation>
  <SIRET-lieu-de-formation>
    <SIRET>...</SIRET> <!-- [1,1] -->
    <extras>...</extras> <!-- [0,N] -->
  </SIRET-lieu-de-formation> <!-- [0,1] -->
  ...
</lieu-de-formation>
```