import re
import subprocess

import gitlab

gl = gitlab.Gitlab.from_config('gitlab', ['/Users/gasilber/.python-gitlab.cfg'])
gl.auth()
user = gl.user

project_name_with_namespace = "lheo/lheo-schema"
project = gl.projects.get(project_name_with_namespace)

open_issues = project.issues.list(state="opened",
            order_by="due_date", sort="asc", milestone="LHÉO 2.3", per_page=100)

issues_list = []
for issue in open_issues:
    issues_list.append(issue)

one_digit_pel_re = re.compile('PEL-(?P<pelnum>\d)')
two_digit_pel_re = re.compile('PEL-(?P<pelnum>\d{2})')
three_digit_pel_re = re.compile('PEL-(?P<pelnum>\d{3})')
four_digit_pel_re = re.compile('PEL-(?P<pelnum>\d{4})')

def key(issue):
    t = issue.title
    m = four_digit_pel_re.search(t)
    if m is not None:
        return 'PEL-%s' % m.group('pelnum')
    m = three_digit_pel_re.search(t)
    if m is not None:
        return 'PEL-0%s' % m.group('pelnum')
    m = two_digit_pel_re.search(t)
    if m is not None:
        return 'PEL-00%s' % m.group('pelnum')
    m = one_digit_pel_re.search(t)
    if m is not None:
        return 'PEL-000%s' % m.group('pelnum')
    raise ValueError("no key found")

issues_list.sort(key=key)

count = len(issues_list)
print(count)


with open("issues.tex", "w") as f:
    for issue in issues_list:
        k = key(issue)
        print(k, issue.title)
        labels = list(
            map(lambda x: '\\textsc{%s}' % x,
                filter(lambda x: x not in {'PEL'},
                    map(lambda x: x.replace('::', '-'),
                        map(str, issue.labels)))))
        print(labels)
        labels.sort()
        url = issue.web_url
        print(url)
        print(issue)
        print()
        f.write("\\section{%s}\n\\label{pel:%s}\n\n" % (issue.title, k))
        f.write("\\noindent\\url{%s}\n\n" % url)
        f.write("\\noindent%s\n\n" % (', '.join(labels)))
        ptexfile = "%s.pandoc.tex" % k
        texfile = "%s.tex" % k
        f.write("\\input{%s}\n\n" % texfile)
        mdfile = "%s.md" % k
        with open(mdfile, "w") as pelf:
            pelf.write(issue.description)
        args = [
            'pandoc',
            '--shift-heading-level-by=1',
            '-t', 'latex',
            '-f', 'gfm',
            '-o', ptexfile,
            mdfile
        ]
        output = subprocess.run(args, capture_output=True)
        if output.returncode != 0:
            raise RuntimeError("cannot execute: %s" % ' '.join(args))
        with open(ptexfile, "r") as paf:
            pcontent = paf.read()
        label_re = re.compile(r'\\(?P<kind>(label|hypertarget)){(?P<ref>[^}]+)}')
        pcontent = label_re.sub(r'\\\g<kind>{%s:\g<ref>}' % k, pcontent)
        href_re = re.compile(r'\\href{(?P<ref>[^}]+)}{(?P<ref2>[^}]+)}')
        pcontent = href_re.sub(r'\\url{\g<ref>}', pcontent)
        with open(texfile, "w") as tf:
            tf.write(pcontent)

