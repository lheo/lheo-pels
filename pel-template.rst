=============================================
PEL 123 - Nouvel élément "Nom nouvel élément"
=============================================

:Auteurs: *liste des auteurs* 
:Demandeurs: *liste des demandeurs*
:Statut: *brouillon* ou *final*
:Création: *date de création au format YYYY-MM-DD*
:Modification: *date de modification au format YYYY-MM-DD*
:Priorité MoSCoW: *M* (Must), *S* (Should), *C* (Could) ou *W* (Won't)
:Impact SI producteur: *faible*, *moyen* ou *fort*
:Impact SI collecteur: *faible*, *moyen* ou *fort*
:Impact SI diffuseur: *faible*, *moyen* ou *fort*

Motivation et description
=========================

Contexte.

Quelle nouvelle information.

Pourquoi.

Identification des consommateurs et producteurs de l'information. 

Législation ou réglementation associée
======================================

Décrêt 2021-3 du 1er janvier 2021.

Évolutions proposées
====================

Ajout d'un élément ``nom-nouvel-element`` 
-----------------------------------------

:Nom de l’élément: ``nom-nouvel-element``
:Type de donnée: texte (250)
:Intitulé: Nouvel élément
:Description: Mettre ici la document de l'élément
:Cercle cible: 1
:Cardinalité: 1 
:Élément parent: ``action``
:Id. glossaire: ``nouvelle-donnee``
:Id. table: sans objet.

Table de référence (optionnel)
++++++++++++++++++++++++++++++

Sans objet.

Exemple de contenu
++++++++++++++++++

.. code-block:: xml

    <action>
        ...
        <nom-nouvel-element>Ceci est un contenu</nom-nouvel-element>
        ...
    </action>


Entrée de glossaire associée et norme AFNOR (optionnel)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++

Nouvelle entrée de glossaire ``nouvelle donnee``.

    **Nouvelle donnée**: description métier de la donnée.

Impacts
=======

Impacts sur les producteurs
---------------------------

Impacts sur le système d'information (côté producteur).
Est-ce que le producteur a cette information?

Impacts sur les systèmes d'information collecteurs
--------------------------------------------------

Impacts sur la collecte (dimension métier).
Est-ce que les SI des collecteurs sont capables d'ingérer et de restituer cette
information?

Impacts sur les systèmes d'information diffuseurs
-------------------------------------------------

Est-ce que les diffuseurs sont capables de traiter cette information et de la
transmettre au public ciblé?





