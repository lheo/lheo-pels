# PEL-2 Capacité minimale d’accueil garantie et Capacité prévisionnelle maximale

- **PEL**: 2
- **Auteurs**: Christophe Bonraisin <christ.bonraisin@pole-emploi.fr>, Mission Apprentissage, Corinne Haranger, Deborah MARCHAND, Cariforef PDL, Nathalie LAVOLO, Carif-Oref Normandie pour RCO 
- **Demandeurs**: Groupe Lheo Apprentissage
- **Statut**: Brouillon
- **Création**: 2021-01-18
- **Modification**: 2021-03-11 
- **Priorité MoSCoW**: M
- **Impact SI producteur**: faible
- **Impact SI collecteur**: fort
- **Impact SI diffuseur**: moyen

## Motivation et description

à compléter

## Législation ou réglementation associée

à compléter

## Évolution proposée

à compléter

## Exemple de contenu

à compléter

## Impacts

### Impacts sur les producteurs

à compléter

### Impacts sur les systèmes d'information collecteurs

à compléter

### Impacts sur les systèmes d'information diffuseurs

à compléter
